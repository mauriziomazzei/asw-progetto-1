package asw.model;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import asw.model.exception.ManufacturerNotFoundException;

@Component
@ConfigurationProperties(prefix = "app")
public class ManufacturerFeatures {
	Random randomSource;
	
	public ManufacturerFeatures() {
		this.randomSource = new Random();
	}
	
	private HashMap<String, List<String>> features;

	public HashMap<String, List<String>> getFeatures() {
		return features;
	}

	public void setFeatures(HashMap<String, List<String>> features) {
		this.features = features;
	}

	public String getRandomFeature(String manufacturer) throws ManufacturerNotFoundException {
		List<String> manufacturerFeatures = this.getFeatures().get(manufacturer);
		if (manufacturerFeatures == null) {
			throw new ManufacturerNotFoundException();
		}
		Integer randomIndex = this.randomSource.nextInt(manufacturerFeatures.size());
		return manufacturerFeatures.get(randomIndex);
	}
}