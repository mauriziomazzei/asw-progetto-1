package asw.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomErrorController implements ErrorController {	
	private static final String ERROR_PATH = "/error";
	
	@RequestMapping(ERROR_PATH)
	public ResponseEntity<?> handleError() {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
	}
	
	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

}
