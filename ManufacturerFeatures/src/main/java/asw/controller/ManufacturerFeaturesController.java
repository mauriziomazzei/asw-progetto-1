package asw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import asw.model.ManufacturerFeatures;
import asw.model.exception.ManufacturerNotFoundException;

@RestController
public class ManufacturerFeaturesController {
	
	@Autowired
	private ManufacturerFeatures manufacturerFeatures;

	@RequestMapping("/manufacturer/{manufacturer}")
	public ResponseEntity<String> getFeature(@PathVariable String manufacturer) throws ManufacturerNotFoundException {
		manufacturer = manufacturer.toLowerCase();
		String randomFeature = this.manufacturerFeatures.getRandomFeature(manufacturer);
		return ResponseEntity.ok(randomFeature);
	}
	
	@ExceptionHandler(ManufacturerNotFoundException.class)
	public ResponseEntity<String> handleManufacturerNotFoundExpection() {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
}
