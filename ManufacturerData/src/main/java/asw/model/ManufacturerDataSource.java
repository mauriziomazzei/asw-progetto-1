package asw.model; 

import java.util.Locale;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import asw.exception.ManufacturerNotFoundException;

@Component
@ConfigurationProperties(prefix = "app")
public class ManufacturerDataSource {
	private static final int MAX_REVENUE = 10000000;
	/*
	 * This object models all available manufacturer's data specified
	 * on the application.yml.
	 * 
	 * Each manufacturer has these properties:
	 * 		(optional )  revenue: a double number 
	 *  	(mandatory)  devices: a map of <deviceName, deviceSpec>
	 *  
	 */
	private Map<String, Map<String, Object>> manufacturerData;

	public Map<String, Map<String, Object>> getManufacturerData() {
		return manufacturerData;
	}

	public void setManufacturerData(Map<String, Map<String, Object>> manufacturerData) {
		this.manufacturerData = manufacturerData;
	}
	
	private Map<String, Object> getManufacturerProperties(String manufacturer)
			throws ManufacturerNotFoundException {
		Map<String, Object> manufactureProperties = this.manufacturerData.get(manufacturer);
		if(manufactureProperties == null)
			throw new ManufacturerNotFoundException();
		
		return this.manufacturerData.get(manufacturer);
	}
	
	public Double getManufacturerRevenue(String manufacturerName) throws ManufacturerNotFoundException {
		Double revenue = (Double) getManufacturerProperties(manufacturerName).get("revenue");
		if(revenue == null) {
			revenue = Math.random() * MAX_REVENUE;
			revenue = Double.parseDouble(String.format(Locale.US, "%.2f", revenue));
		}
		return revenue;
	}
	
	public Map<String, String> getManufacturerDevicesCatalog(String manufacturerName) throws ManufacturerNotFoundException {
		Map<String, String> devicesCatalog;
		devicesCatalog = (Map<String, String>) getManufacturerProperties(manufacturerName).get("devices");
		return devicesCatalog;
	}
}
