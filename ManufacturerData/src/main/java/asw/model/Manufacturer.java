package asw.model;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import asw.exception.DeviceNotFoundException;

@Component
@ConfigurationProperties(prefix = "app")
public class Manufacturer {
	
	private Double revenue;
	
	private Map<String, String> devicesCatalog;

	public Double getRevenue() {
		return this.revenue;
	}

	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	
	public Map<String, String> getDevicesCatalog() {
		return this.devicesCatalog;
	}
	
	public String  getDeviceInfo(String deviceName) throws DeviceNotFoundException {
		String deviceInfo = this.getDevicesCatalog().get(deviceName);
		if(deviceInfo == null) {
			throw new DeviceNotFoundException();
		}
		return deviceInfo;
	}

	public void setDeviceCatalog(Map<String, String> devicesCatalog) {
		this.devicesCatalog = devicesCatalog;
	}

	
}