package asw.model;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import asw.exception.ManufacturerNotFoundException;

@Component
public class ManufacturerFactory {
	
	@Autowired
	private ManufacturerDataSource manufacturerDataSource;
	
	public Manufacturer getManufacturer(String manufacturerName) throws ManufacturerNotFoundException {
		Map<String, String> devicesCatalog = manufacturerDataSource.getManufacturerDevicesCatalog(manufacturerName);
		Double revenue = manufacturerDataSource.getManufacturerRevenue(manufacturerName);

		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setRevenue(revenue);
		manufacturer.setDeviceCatalog(devicesCatalog);
		
		return manufacturer;
	}
	
}
