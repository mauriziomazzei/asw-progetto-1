package asw.exception;

public class ManufacturerNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ManufacturerNotFoundException() {
		super("manufacturer not found");
	}
}
