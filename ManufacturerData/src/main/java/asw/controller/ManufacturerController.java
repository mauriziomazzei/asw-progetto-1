package asw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import asw.model.ManufacturerFactory;
import asw.exception.DeviceNotFoundException;
import asw.exception.ManufacturerNotFoundException;
import asw.model.Manufacturer;

@RestController
public class ManufacturerController {
	
	@Autowired
	private ManufacturerFactory manufacturerFactory;

	@RequestMapping("/manufacturer/{manufacturerName}")
	public ResponseEntity<Double> getFeature(@PathVariable String manufacturerName) throws ManufacturerNotFoundException {
		manufacturerName = manufacturerName.toLowerCase();
		
		Manufacturer manufacturer = manufacturerFactory.getManufacturer(manufacturerName);
		
		return ResponseEntity.ok(manufacturer.getRevenue());
	}
	
	@RequestMapping("/manufacturer/{manufacturerName}/device/{deviceName}")
	public ResponseEntity<String> getDeviceInfo(@PathVariable String manufacturerName,
											    @PathVariable String deviceName) 
													 throws ManufacturerNotFoundException, DeviceNotFoundException {
		manufacturerName = manufacturerName.toLowerCase();
		deviceName = deviceName.toLowerCase();
		
		Manufacturer manufacturer = manufacturerFactory.getManufacturer(manufacturerName);
		
		return ResponseEntity.ok(manufacturer.getDeviceInfo(deviceName));
	}
	
	@ExceptionHandler(ManufacturerNotFoundException.class)
	public ResponseEntity<String> handleManufacturerNotFoundExpection(Exception e) {
		String errorMessage = e.getMessage();
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
	}
	
	@ExceptionHandler(DeviceNotFoundException.class)
	public ResponseEntity<String> handleDeviceNotFoundExpection(Exception e) {
		String errorMessage = e.getMessage();
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
	}
	
}
