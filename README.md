# Manufacturer Application

A simple distributed application using Spring Cloud framework and Docker Swarm.

## build

To build all applications run:
```sh
    ./build_all.sh
```
To create a Docker container for each application run:

**NOTE**: all the scripts are configured to talk with a docker repository hosted
  on `swarm.inf.uniroma3.it:5000`

```sh
    ./build_all_images.sh
```

## deploy

To push all images on the repository and then run the application:
```sh
    ./push_all_images.sh
    ./start-manufacturer-stack.sh
```

After ~1 minutes, the application can be reached on `swarm.inf.uniroma3.it:9011`.

Exposed API:

	http://swarm.inf.uniroma3.it:9011/manufacturer/\<manufacturerName\>
	http://swarm.inf.uniroma3.it:9011/manufacturer/\<manufacturerName\>/device/\<deviceName\>

Examples:
- http://swarm.inf.uniroma3.it:9011/manufacturer/samsung
- http://swarm.inf.uniroma3.it:9011/manufacturer/samsung/device/s8

## notes

The API Gateway (Zuul) is configured in such a way that only the desidered API
is exposed (*ManufacturerApplication*).

Other endpoints, such as *ManufacturerData* and *ManufacturerFeatures*, can't be
reached directly.