#!/bin/bash

source "docker.env"

CODENAME=9011
#DOCKER_REGISTRY=localhost:5000
DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000

docker build --rm -t ${DOCKER_REGISTRY}/eureka-server-$CODENAME ./EurekaServer
docker build --rm -t ${DOCKER_REGISTRY}/manufacturer-application-$CODENAME ./ManufacturerApplication
docker build --rm -t ${DOCKER_REGISTRY}/manufacturer-data-$CODENAME ./ManufacturerData
docker build --rm -t ${DOCKER_REGISTRY}/manufacturer-features-$CODENAME ./ManufacturerFeatures
docker build --rm -t ${DOCKER_REGISTRY}/zuul-$CODENAME ./Zuul
