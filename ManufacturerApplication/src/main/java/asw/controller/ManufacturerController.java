package asw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import asw.client.*;
import asw.service.ManufacturerService;

@RestController
public class ManufacturerController {
	
	@Autowired
	private ManufacturerService manufacturerService;
	
	@RequestMapping("/manufacturer/{manufacturerName}")
	public ResponseEntity<String> getManufacturer(@PathVariable String manufacturerName) {
		
		Double revenue = manufacturerService.getRevenue(manufacturerName);
		String feature = manufacturerService.getFeature(manufacturerName);
		
		String body = String.format("%s %s ed ha un fatturato di %.2f.", manufacturerName, feature, revenue);
		return ResponseEntity.ok().body(body);
	}
	
	@RequestMapping("/manufacturer/{manufacturerName}/device/{deviceName}")
	public ResponseEntity<String> getDevice(@PathVariable String manufacturerName,
											@PathVariable String deviceName) {
		
		String feature = manufacturerService.getFeature(manufacturerName);
		String deviceInfo = manufacturerService.getDeviceInfo(manufacturerName, deviceName); 
		
		String body = String.format("%s %s, %s ha %s.", manufacturerName, feature, deviceName, deviceInfo);
		return ResponseEntity.ok().body(body);
	}
	
	
	@ExceptionHandler(HttpClientErrorException.class)
	public ResponseEntity<String> errorExceptionHandler(HttpClientErrorException exception) {
		HttpStatus status = exception.getStatusCode();
		String body = exception.getResponseBodyAsString();
		return ResponseEntity.status(status).body(body);
	}
	
	
}
