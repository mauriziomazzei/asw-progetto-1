package asw.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("ManufacturerData")
public interface ManufacturerDataClient {
	
	@RequestMapping(value="/manufacturer/{manufacturerName}", method=RequestMethod.GET)
	public Double getRevenue(@PathVariable("manufacturerName") String manufacturerName);
	
	@RequestMapping(value="/manufacturer/{manufacturerName}/device/{deviceName}", method=RequestMethod.GET)
	public String getDeviceInfo(@PathVariable("manufacturerName") String manufacturerName, 
							    @PathVariable("deviceName") String deviceName) ;
}
