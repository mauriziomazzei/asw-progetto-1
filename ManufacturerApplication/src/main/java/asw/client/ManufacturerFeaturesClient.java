package asw.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("ManufacturerFeatures")
public interface ManufacturerFeaturesClient {
	
	@RequestMapping(value="/manufacturer/{manufacturerName}", method=RequestMethod.GET)
	public String getFeature(@PathVariable("manufacturerName") String manufacturerName);

}
