package asw.service;

public interface ManufacturerService {
	
	public String getFeature(String manufacturerName);
	public Double getRevenue(String manufacturerName);
	public String getDeviceInfo(String manufacturerName, String deviceName);

}
