package asw.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import asw.client.ManufacturerDataClient;
import asw.client.ManufacturerFeaturesClient;

@Component
public class ManufacturerServiceImpl implements ManufacturerService {
	@Autowired
	private ManufacturerFeaturesClient manufacturerFeaturesClient;
	
	@Autowired
	private ManufacturerDataClient manufacturerDataClient;
	
	@HystrixCommand(fallbackMethod="getFallbackFeature")
	public String getFeature(String manufacturerName) {
		return manufacturerFeaturesClient.getFeature(manufacturerName); 
	}

	@HystrixCommand(fallbackMethod="getFallbackRevenue")
	public Double getRevenue(String manufacturerName) {
		return manufacturerDataClient.getRevenue(manufacturerName);
	}

	@HystrixCommand(fallbackMethod="getFallbackDeviceInfo")
	public String getDeviceInfo(String manufacturerName, String deviceName) {
		return manufacturerDataClient.getDeviceInfo(manufacturerName, deviceName);
	}
	
	public String getFallbackFeature(String manufacturerName) {
		return "produce telefoni";
	}
	
	public String getFallbackDeviceInfo(String manufacturerName, String deviceName) {
		return "CPU, RAM";
	}
	
	public Double getFallbackRevenue(String manufacturerName) {
		return 0.0;
	}
}
