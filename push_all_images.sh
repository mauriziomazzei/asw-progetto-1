#!/bin/bash

source "docker.env"

CODENAME=9011
#DOCKER_REGISTRY=localhost:5000
DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000

docker push ${DOCKER_REGISTRY}/eureka-server-$CODENAME
docker push ${DOCKER_REGISTRY}/manufacturer-application-$CODENAME
docker push ${DOCKER_REGISTRY}/manufacturer-data-$CODENAME
docker push ${DOCKER_REGISTRY}/manufacturer-features-$CODENAME
docker push ${DOCKER_REGISTRY}/zuul-$CODENAME
